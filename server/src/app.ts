import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import routes from './routes/index';

const app = express();
const PORT = 8080;

// const {
//     MONGODB_ATLAS_USERNAME,
//     MONGODB_ATLAS_PASSWORD,
//     MONGODB_ATLAS_DBNAME,
// } = process.env;

// const uri = `mongodb+srv://${MONGODB_ATLAS_USERNAME}:${MONGODB_ATLAS_PASSWORD}@cluster0.tw8dl.mongodb.net/${MONGODB_ATLAS_DBNAME}?retryWrites=true&w=majority`
// const uri = `mongodb+srv://wahyu:<password>@cluster0.tw8dl.mongodb.net/<dbname>?retryWrites=true&w=majority`
const uri = 'mongodb://localhost/dblatihan001';
console.info(uri);

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true
};

app.use(cors());

app.use(routes);

mongoose.set('useFindAndModify', true);
mongoose
  .connect(uri, options)
  .then(() => {
    app.listen(PORT, () => {
      console.info(`App listening at port http://localhost:${PORT}`);
    });
  })
  .catch((err) => {
    throw err;
  });
